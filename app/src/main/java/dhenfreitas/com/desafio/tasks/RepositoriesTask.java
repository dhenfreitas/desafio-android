package dhenfreitas.com.desafio.tasks;

import android.app.Activity;
import android.os.AsyncTask;

import dhenfreitas.com.desafio.activities.MainActivity;
import dhenfreitas.com.desafio.interfaces.GitHubAPIs;
import dhenfreitas.com.desafio.models.Repositories;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Diego on 06/12/2017.
 */

public class RepositoriesTask extends AsyncTask<String, Void, Void> {

    private Activity activity;
    private int currentPage;

    public RepositoriesTask(Activity activity, int currentPage) {
        this.activity = activity;
        this.currentPage = currentPage;
    }

    @Override
    protected Void doInBackground(String... params) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(GitHubAPIs.SEARCH_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        GitHubAPIs gitHubAPIs = retrofit.create(GitHubAPIs.class);
        Call<Repositories> call = gitHubAPIs.getRepositories("Java", "stars", currentPage);
        call.enqueue(new Callback<Repositories>() {
            @Override
            public void onResponse(Call<Repositories> call, retrofit2.Response<Repositories> response) {
                if(response.code() != 200) {
                    // Error handler


                } else {
                    Repositories repositories = response.body();

                    if(repositories != null ) {

                        ((MainActivity) activity).addNewRepositories(repositories.getRepositories());
                    }

                    if((currentPage * 30) < repositories.getTotal_count()) {
                        RepositoriesTask repositoriesTask = new RepositoriesTask(activity, currentPage+1);
                        repositoriesTask.execute();
                    }


                }
            }

            @Override
            public void onFailure(Call<Repositories> call, Throwable t) {

            }
        });

        return null;
    }
}
