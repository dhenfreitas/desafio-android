package dhenfreitas.com.desafio.interfaces;

import java.util.List;

import dhenfreitas.com.desafio.models.PullRequest;
import dhenfreitas.com.desafio.models.Repositories;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by Diego on 05/12/2017.
 */

public interface GitHubAPIs {

    String SEARCH_BASE_URL = "https://api.github.com/search/";
    String REPOSITORY_BASE_URL = "https://api.github.com/repos/";

    @Headers("Content-Type: application/json")
    @GET("repositories?")
    Call<Repositories> getRepositories(@Query("q") String language, @Query("sort") String sort, @Query("page") int page);

    @Headers("Content-Type: application/json")
    @GET("pulls")
    Call<List<PullRequest>> getPullRequests();

}
