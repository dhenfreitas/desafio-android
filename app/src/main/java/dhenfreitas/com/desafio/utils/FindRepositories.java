package dhenfreitas.com.desafio.utils;

import android.app.Activity;

import dhenfreitas.com.desafio.activities.MainActivity;
import dhenfreitas.com.desafio.interfaces.GitHubAPIs;
import dhenfreitas.com.desafio.models.Repositories;
import dhenfreitas.com.desafio.tasks.RepositoriesTask;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Diego on 06/12/2017.
 */

public class FindRepositories {

    Activity activity;

    public void buildRepositoiesList(final Activity activity) {
        this.activity = activity;

        if(Util.isNetworkAvailable(activity)) {

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(GitHubAPIs.SEARCH_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            GitHubAPIs gitHubAPIs = retrofit.create(GitHubAPIs.class);
            Call<Repositories> call;
//            if(movieID != null || movieID.length() > 0) {
//                call = gitHubAPIs.getUpcomingMovies("1f54bd990f1cdfb230adb312546d765d", "en-US", 1);
//            } else {
//                call = gitHubAPIs.getUpcomingMovies("1f54bd990f1cdfb230adb312546d765d", "en-US", 1);
//            }

            call = gitHubAPIs.getRepositories("Java", "stars", 1);

            call.enqueue(new Callback<Repositories>() {
                @Override
                public void onResponse(Call<Repositories> call, retrofit2.Response<Repositories> response) {

                    if(response.code() != 200) {
                        // Error handler


                    } else {
                        Repositories repositories = response.body();

                        if(repositories != null ) {

                            ((MainActivity) activity).addNewRepositories(repositories.getRepositories());

                            RepositoriesTask repositoriesTask = new RepositoriesTask(activity, 1);
                            repositoriesTask.execute();
                        }
                    }

                }

                @Override
                public void onFailure(Call<Repositories> call, Throwable t) {

                }
            });
        } else {
            // Sem internet

        }

    }
}
