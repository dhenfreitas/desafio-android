package dhenfreitas.com.desafio.utils;

/**
 * Created by Diego on 05/12/2017.
 */

public class Verbose {

    public static final String LATO_LIGHT = "LATO LIGHT";
    public static final String LATO_BLACK = "LATO BLACK";
    public static final String AUTHOR_NAME = "AUTHOR NAME";
    public static final String REPOSITORY_NAME = "REPOSITORY NAME";
    public static final String SMALL_REFERENCE = "SMALL REFERENCE";
    public static final String FIND_MOVIES_FRAGMENT = "FIND MOVIES FRAGMENT";
    public static final String MOVIE_DB_IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w500/";

}
