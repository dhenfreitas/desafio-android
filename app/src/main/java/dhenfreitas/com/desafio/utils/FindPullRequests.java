package dhenfreitas.com.desafio.utils;

import android.app.Activity;

import java.util.List;

import dhenfreitas.com.desafio.activities.MainActivity;
import dhenfreitas.com.desafio.activities.RepositoryActivity;
import dhenfreitas.com.desafio.interfaces.GitHubAPIs;
import dhenfreitas.com.desafio.models.PullRequest;
import dhenfreitas.com.desafio.models.Repositories;
import dhenfreitas.com.desafio.tasks.RepositoriesTask;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Diego on 06/12/2017.
 */

public class FindPullRequests {

    Activity activity;

    public void buildPullRequestsList(final Activity activity, String authorName, String repositoryName) {
        this.activity = activity;

        if(Util.isNetworkAvailable(activity)) {

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(GitHubAPIs.REPOSITORY_BASE_URL+repositoryName+"/"+authorName+"/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            GitHubAPIs gitHubAPIs = retrofit.create(GitHubAPIs.class);
            Call<List<PullRequest>> call;

            call = gitHubAPIs.getPullRequests();

            call.enqueue(new Callback<List<PullRequest>>() {
                @Override
                public void onResponse(Call<List<PullRequest>> call, retrofit2.Response<List<PullRequest>> response) {

                    if(response.code() != 200) {
                        // Error handler


                    } else {
                        List<PullRequest> repositories = response.body();

                        if(repositories != null ) {

                            ((RepositoryActivity) activity).addNewRepositories(repositories);
                        }
                    }

                }

                @Override
                public void onFailure(Call<List<PullRequest>> call, Throwable t) {

                }
            });
        } else {
            // Sem internet

        }

    }
}
