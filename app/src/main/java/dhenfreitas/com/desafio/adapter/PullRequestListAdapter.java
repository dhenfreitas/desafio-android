package dhenfreitas.com.desafio.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import dhenfreitas.com.desafio.R;
import dhenfreitas.com.desafio.models.PullRequest;
import dhenfreitas.com.desafio.utils.Util;

/**
 * Created by Diego on 06/12/2017.
 */

public class PullRequestListAdapter extends ArrayAdapter<PullRequest> {

    private List<PullRequest> repositoriesList;
    private Activity activity;
    private Filter resultFilter;
    private List<PullRequest> origRepositoriesList;

    public PullRequestListAdapter(List<PullRequest> repositoriesList, Activity activity) {
        super(activity, R.layout.row_pull_request_list_layout, repositoriesList);
        this.repositoriesList = repositoriesList;
        this.activity = activity;
        this.origRepositoriesList = repositoriesList;
    }

    public int getCount() {
        return repositoriesList.size();
    }

    public PullRequest getItem(int position) {
        return repositoriesList.get(position);
    }

    public long getItemId(int position) {
        return repositoriesList.get(position).hashCode();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        RepositoryHolder holder = new RepositoryHolder();

        // First let's verify the convertView is not null
        if (convertView == null) {
            // This a new view we inflate the new layout
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.row_pull_request_list_layout, null);

            // Now we can fill the layout with the right values
            ImageView userAvatar = (ImageView) v.findViewById(R.id.avatar);
            TextView userName = (TextView) v.findViewById(R.id.autor);

            TextView repositoryName = (TextView) v.findViewById(R.id.name);
            TextView repositoryDescription = (TextView) v.findViewById(R.id.description);
            TextView repositoryForks = (TextView) v.findViewById(R.id.forks);
            TextView repositoryStars = (TextView) v.findViewById(R.id.stars);

            holder.userAvatar = userAvatar;
            holder.userName = userName;
            holder.repositoryName = repositoryName;
            holder.repositoryDescription = repositoryDescription;


            v.setTag(holder);
        }
        else
            holder = (RepositoryHolder) v.getTag();

        PullRequest r = repositoriesList.get(position);

        String imageURL = r.getUser().getAvatar_url();

        Util.setImage(activity, holder.userAvatar, imageURL);

        holder.userName.setText(r.getUser().getLogin());

        holder.repositoryName.setText(r.getTitle());

        holder.repositoryDescription.setText(r.getBody() + "\n");

        return v;
    }

    // Holder for faster viewing and protect the component
    private static class RepositoryHolder {
        public ImageView userAvatar;
        public TextView userName;
        public TextView repositoryName;
        public TextView repositoryDescription;
    }
}
