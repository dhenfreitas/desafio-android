package dhenfreitas.com.desafio.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import dhenfreitas.com.desafio.R;
import dhenfreitas.com.desafio.adapter.PullRequestListAdapter;
import dhenfreitas.com.desafio.adapter.RepositoryListAdapter;
import dhenfreitas.com.desafio.models.PullRequest;
import dhenfreitas.com.desafio.models.Repository;
import dhenfreitas.com.desafio.utils.FindPullRequests;
import dhenfreitas.com.desafio.utils.FindRepositories;
import dhenfreitas.com.desafio.utils.Verbose;

public class RepositoryActivity extends AppCompatActivity {

    InputMethodManager imm;
    private Toolbar mToolbar;
    private String authorName, repositoryName;

    // The data to show
    List<PullRequest> repositories = new ArrayList<>();
    PullRequestListAdapter repositoryListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repository);

        Bundle extras = getIntent().getExtras();

        authorName = extras.getString(Verbose.AUTHOR_NAME);

        repositoryName = extras.getString(Verbose.REPOSITORY_NAME);

        mToolbar = findViewById(R.id.toolbar);

        mToolbar.setTitle(repositoryName);

        mToolbar.setNavigationIcon(R.drawable.ic_action_name);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

        String movieID = "";

        FindPullRequests findPullRequests = new FindPullRequests();
        findPullRequests.buildPullRequestsList(this, authorName, repositoryName);

        // We get the ListView component from the layout
        ListView lv = (ListView) findViewById(R.id.listView);

        // This adapter will receive the first list of movies
        // Newly retrieved movies will be added continuously
        repositoryListAdapter = new PullRequestListAdapter(repositories, this);
        lv.setAdapter(repositoryListAdapter);

        // we register for the contextmneu
        registerForContextMenu(lv);

        // TextFilter
        lv.setTextFilterEnabled(true);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

        return true;
    }


    public void addNewRepositories(List<PullRequest> repositories) {

        int opened = 0, closed = repositories.size();
        // Incrementing the list of movies
        for(int i = 0; i < repositories.size(); i++) {
            RepositoryActivity.this.repositories.add(repositories.get(i));
            RepositoryActivity.this.repositoryListAdapter.notifyDataSetChanged(); // notify when the data model is changed

            if(repositories.get(i).getState().equals("open")) {
                opened++;
            }
        }

        TextView openedLabel = (TextView) findViewById(R.id.opened);

        openedLabel.setText(opened+" opened ");

        TextView closedLabel = (TextView) findViewById(R.id.closed);

        closedLabel.setText("/ "+closed+" closed");
    }
}
