package dhenfreitas.com.desafio.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import dhenfreitas.com.desafio.R;
import dhenfreitas.com.desafio.adapter.RepositoryListAdapter;
import dhenfreitas.com.desafio.models.Repository;
import dhenfreitas.com.desafio.utils.FindRepositories;
import dhenfreitas.com.desafio.utils.Verbose;

public class MainActivity extends AppCompatActivity {

    InputMethodManager imm;
    private Toolbar mToolbar;

    // The data to show
    List<Repository> repositories = new ArrayList<>();
    RepositoryListAdapter repositoryListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToolbar = findViewById(R.id.toolbar);

        mToolbar.setNavigationIcon(R.drawable.ic_menu);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        String movieID = "";

        FindRepositories findRepositories = new FindRepositories();
        findRepositories.buildRepositoiesList(this);

        // We get the ListView component from the layout
        ListView lv = (ListView) findViewById(R.id.listView);

        // This adapter will receive the first list of movies
        // Newly retrieved movies will be added continuously
        repositoryListAdapter = new RepositoryListAdapter(repositories, this);
        lv.setAdapter(repositoryListAdapter);

        // React to user clicks on item
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parentAdapter, View view, int position,
                                    long id) {

                imm = (InputMethodManager)getSystemService(Context.
                        INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

                // The view is a Linear Layout whose children are:
                LinearLayout layout = ((LinearLayout) view);

                // [0] Linear Leayout with repository info
                LinearLayout layoutChild = (LinearLayout) layout.getChildAt(0);

                // [0] authorName
                TextView autorName = (TextView) layoutChild.getChildAt(0);

                // [1] Linear Leayout with user info
                layoutChild = (LinearLayout) layout.getChildAt(1);

                // [1] repositoryName
                TextView repositoryName = (TextView) layoutChild.getChildAt(1);

                Intent i = new Intent(MainActivity.this, RepositoryActivity.class);

                i.putExtra(Verbose.AUTHOR_NAME, autorName.getText());

                i.putExtra(Verbose.REPOSITORY_NAME, repositoryName.getText());

                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                MainActivity.this.startActivity(i);

            }
        });

        // we register for the contextmneu
        registerForContextMenu(lv);

        // TextFilter
        lv.setTextFilterEnabled(true);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

        return true;
    }


    public void addNewRepositories(List<Repository> repositories) {

        // Incrementing the list of movies
        for(int i = 0; i < repositories.size(); i++) {
            MainActivity.this.repositories.add(repositories.get(i));
            MainActivity.this.repositoryListAdapter.notifyDataSetChanged(); // notify when the data model is changed
        }
    }

    public void onSaveInstanceState(Bundle savedState) {

        super.onSaveInstanceState(savedState);


        List<Repository> values = repositories;
        savedState.putSerializable("myKey", (Serializable) values);

    }
}
