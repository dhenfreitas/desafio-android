package dhenfreitas.com.desafio.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Diego on 06/12/2017.
 */

public class Repositories {
    @SerializedName("total_count")
    @Expose
    private int total_count;

    @SerializedName("incomplete_results")
    @Expose
    private boolean incomplete_results;

    @SerializedName("items")
    @Expose
    private List<Repository> repositories;

    public int getTotal_count() {
        return total_count;
    }

    public void setTotal_count(int total_count) {
        this.total_count = total_count;
    }

    public boolean isIncomplete_results() {
        return incomplete_results;
    }

    public void setIncomplete_results(boolean incomplete_results) {
        this.incomplete_results = incomplete_results;
    }

    public List<Repository> getRepositories() {
        return repositories;
    }

    public void setRepositories(List<Repository> repositories) {
        this.repositories = repositories;
    }
}
