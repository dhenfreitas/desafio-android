package dhenfreitas.com.desafio.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Diego on 06/12/2017.
 */

public class Repository implements Serializable {
    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("full_name")
    @Expose
    private String full_name;

    @SerializedName("owner")
    @Expose
    private Owner owner;

//    @SerializedName("private")
//    @Expose
//    private boolean privacity;
//
//    @SerializedName("html_url")
//    @Expose
//    private String html_url;

    @SerializedName("description")
    @Expose
    private String description;

//    @SerializedName("fork")
//    @Expose
//    private boolean fork;
//
//    @SerializedName("url")
//    @Expose
//    private String url;
//
//    @SerializedName("forks_url")
//    @Expose
//    private String forks_url;
//
//    @SerializedName("keys_url")
//    @Expose
//    private String keys_url;
//
//    @SerializedName("collaborators_url")
//    @Expose
//    private String collaborators_url;
//
//    @SerializedName("teams_url")
//    @Expose
//    private String teams_url;
//
//    @SerializedName("hooks_url")
//    @Expose
//    private String hooks_url;
//
//    @SerializedName("issue_events_url")
//    @Expose
//    private String issue_events_url;
//
//    @SerializedName("events_url")
//    @Expose
//    private String events_url;
//
//    @SerializedName("assignees_url")
//    @Expose
//    private String assignees_url;
//
//    @SerializedName("branches_url")
//    @Expose
//    private String branches_url;
//
//    @SerializedName("tags_url")
//    @Expose
//    private String tags_url;
//
//    @SerializedName("blobs_url")
//    @Expose
//    private String blobs_url;
//
//    @SerializedName("git_tags_url")
//    @Expose
//    private String git_tags_url;
//
//    @SerializedName("git_refs_url")
//    @Expose
//    private String git_refs_url;
//
//    @SerializedName("trees_url")
//    @Expose
//    private String trees_url;
//
//    @SerializedName("statuses_url")
//    @Expose
//    private String statuses_url;
//
//    @SerializedName("languages_url")
//    @Expose
//    private String languages_url;
//
//    @SerializedName("stargazers_url")
//    @Expose
//    private String stargazers_url;
//
//    @SerializedName("contributors_url")
//    @Expose
//    private String contributors_url;
//
//    @SerializedName("subscribers_url")
//    @Expose
//    private String subscribers_url;
//
//    @SerializedName("subscription_url")
//    @Expose
//    private String subscription_url;
//
//    @SerializedName("commits_url")
//    @Expose
//    private String commits_url;
//
//    @SerializedName("git_commits_url")
//    @Expose
//    private String git_commits_url;
//
//    @SerializedName("comments_url")
//    @Expose
//    private String comments_url;
//
//    @SerializedName("issue_comment_url")
//    @Expose
//    private String issue_comment_url;
//
//    @SerializedName("contents_url")
//    @Expose
//    private String contents_url;
//
//    @SerializedName("compare_url")
//    @Expose
//    private String compare_url;
//
//    @SerializedName("merges_url")
//    @Expose
//    private String merges_url;
//
//    @SerializedName("archive_url")
//    @Expose
//    private String archive_url;
//
//    @SerializedName("downloads_url")
//    @Expose
//    private String downloads_url;
//
//    @SerializedName("issues_url")
//    @Expose
//    private String issues_url;
//
//    @SerializedName("pulls_url")
//    @Expose
//    private String pulls_url;
//
//    @SerializedName("milestones_url")
//    @Expose
//    private String milestones_url;
//
//    @SerializedName("notifications_url")
//    @Expose
//    private String notifications_url;
//
//    @SerializedName("labels_url")
//    @Expose
//    private String labels_url;
//
//    @SerializedName("releases_url")
//    @Expose
//    private String releases_url;
//
//    @SerializedName("deployments_url")
//    @Expose
//    private String deployments_url;

    @SerializedName("stargazers_count")
    @Expose
    private int stargazers_count;

    @SerializedName("forks_count")
    @Expose
    private int forks_count;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStargazers_count() {
        return stargazers_count;
    }

    public void setStargazers_count(int stargazers_count) {
        this.stargazers_count = stargazers_count;
    }

    public int getForks_count() {
        return forks_count;
    }

    public void setForks_count(int forks_count) {
        this.forks_count = forks_count;
    }
}
